# QtNet 
```
   ___  _   _   _      _        ____                         _     
  / _ \| |_| \ | | ___| |_     | __ ) _   _    _ __   ___ __| |   
 | | | | __|  \| |/ _ \ __|    |  _ \| | | |  | '_ \ / __/ _` |    
 | |_| | |_| |\  |  __/ |_     | |_) | |_| |  | |_) | (_| (_| |  
  \__\_\\__|_| \_|\___|\__|    |____/ \__, |  | .__/ \___\__,_|  
                                      |___/   |_|                
```
（项目及文档持续完善中，欢迎加入。 交流QQ群 ：876023075）  
## :banana:介绍
使用Qt5框架开发的跨平台网络库，包括：
1. 通用的TCP通信类；  
2. 通用的UDP通信类。

## :pear:案例
* [QPerf](https://gitee.com/andwp/qperf) [![Fork me on Gitee](https://gitee.com/andwp/qperf/widgets/widget_5.svg)](https://gitee.com/andwp/qperf) 基于`QtNet` 开发的跨平台网络性能测试软件。   
* [QSocketAssist(QSA)](https://gitee.com/andwp/qsocket-assist)  [![Fork me on Gitee](https://gitee.com/andwp/qsocket-assist/widgets/widget_5.svg)](https://gitee.com/andwp/qsocket-assist)  基于`QtNet` 开发socket测试软件。   
## :peach:使用说明
参见[使用说明文档](doc/使用说明.md)

## :apple: 软件架构
参见[软件架构文档](doc/软件架构.md)

## 目录结构
- qtsocket 基本的socket通信库项目文件，包括udp通信与tcp通信类；（已完成初版）；
- include socket通信库头文件；
- src socket通信库实现文件；  
- nettest  socket通信库的测试界面。
