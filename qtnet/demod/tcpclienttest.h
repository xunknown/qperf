﻿#ifndef TCPCLIENTTEST_H
#define TCPCLIENTTEST_H

#include <QHostAddress>
#include <QObject>
#include <QTimerEvent>

#include <net/tcpclient.h>

#include <net/netreading.h>
#include <net/netwriting.h>
#include <net/tcpsession.h>


QNET_USING_NAMESPACE

/**
 * @brief The TcpClientTest class 表示tcpClient的类测试。
 */
class TcpClientTest
        : public QObject,
        public NetReading,
        public ISessionListener
{
    Q_OBJECT
public:
    explicit TcpClientTest(QObject *parent = nullptr);
    ~TcpClientTest() override;
    /**
     * @brief bind 绑定本机Tcp地址。
     * @param localAddress
     * @param localPort
     * @return
     */
    bool bind(QHostAddress localAddress, quint16 localPort);
    /**
     * @brief setAutoConnect  设置自动连接。
     * @param isAuto
     */
    void setAutoConnect(bool isAuto);
    /**
     * @brief connect 连接服务端。
     * @param remoteAddres
     * @param remotePort
     */
    void connect(QHostAddress remoteAddress, quint16 remotePort);
    void stop();
signals:
    void log_signal(QString logInfo);
    // TcpReading interface
public:
    void onProc(const QByteArray &arry) override;
    // QObject interface
protected:
    void timerEvent(QTimerEvent *event) override;

    // ISessionListener interface
public:
    void onStateChanged(TcpSession *session, bool connected) override;
    void onError(QAbstractSocket::SocketError error) override;

public slots:
    /**
     * @brief fulfil  写入完毕回调。
     */
    void fulfil(QSharedPointer<NetWriting> sender, int size);
private:
signals:
    void stop_signal();
private slots:
    void stop_slot();
private:
    TcpClient *m_client;
    QThread *m_thread;
    TcpSession *m_session;
    QHostAddress m_localAddress;
    quint16 m_localPort;
    int m_timerID;
    QAtomicInt m_counter; // 数据发送计数器。
   bool m_isAutoConnect; // 是否自动连接。
   bool m_stopped; // 是否关闭。

};

#endif // TCPCLIENTTEST_H
