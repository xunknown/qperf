QT += network

INCLUDEPATH += $$PWD/../include/
INCLUDEPATH += $$PWD/../src/

DEFINES += QNET_BUILD


win32:{
DEFINES += WIN32_LEAN_AND_MEAN
LIBS += -lWs2_32
}

HEADERS += \
      $$PWD/../include/net/netreading.h \
      $$PWD/../include/net/netwriting.h \
      $$PWD/../include/net/tcpclient.h \
      $$PWD/../include/net/tcpserver.h \
      $$PWD/../include/net/tcpsession.h \
      $$PWD/../include/net/udpchannel.h \
      $$PWD/../include/common/observer.h\
      $$PWD/../include/common/taskentry.h\
      $$PWD/../include/common/taskqueue.h\
      $$PWD/../include/qnet_global.h
