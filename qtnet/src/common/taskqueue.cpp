﻿#include <common/taskentry.h>
#include <common/taskqueue.h>
#include <QDebug>

TaskQueue::TaskQueue(int maxSize, bool autoDel):
    m_semaphore(maxSize),
    m_maxSize(maxSize),
    m_autoDel(autoDel)
{
    m_canceled = false;
    m_procing = false;
    m_queue.reserve(m_maxSize);

    this->setAutoDelete(false);
}

TaskQueue::~TaskQueue()
{
    cancel();
}

bool TaskQueue::add(TaskEntry *entry)
{
    if(m_canceled)
    {
        return false;
    }
    //添加队列，请求一个信号量。
    if(false == m_semaphore.tryAcquire())
    {
        return false;
    }
    m_muxtex.lock();
    m_queue.enqueue(entry);
    if(!m_procing)
    {
        m_procing = true;
        QThreadPool *threadPool = QThreadPool::globalInstance();
        threadPool->start(this);
    }
    m_muxtex.unlock();
    return true;
}

void TaskQueue::cancel()
{
    if(!m_canceled) //  已取消不能再次取消。不然会无限等待。
    {
        m_canceled = true;
        // 等待信号量
        m_semaphore.acquire(m_maxSize);
    }
}

int TaskQueue::size() const
{
    return m_queue.size();
}

void TaskQueue::excute()
{
    TaskEntry *entry = nullptr;
    while(!m_queue.isEmpty())
    {
        // 处理实例出队
        m_muxtex.lock();
        if(!m_queue.isEmpty())
        {
            entry = m_queue.dequeue();
        }
        m_muxtex.unlock();

        //执行处理实例。
        if(entry!=nullptr)
        {
            if(!m_canceled)
            {
                entry->onProc();
            }
            else
            {
                entry->onDiscard();
            }

            // 自动删除。
            if(m_autoDel)
            {
                delete entry;
            }
            entry = nullptr;

            // 执行完毕，释放信号。
            m_semaphore.release();
        }
    }
    m_procing = false;
}

void TaskQueue::run()
{
    excute();
}

