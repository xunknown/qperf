﻿#ifndef REPLYPACKETWRITER_H
#define REPLYPACKETWRITER_H

#include "idatawriter.h"
 
class ReplyPacket;

  
 
/**
 * @brief 表示回复数据包实体编码器。
 */
class ReplyPacketWriter : public IDataWriter<ReplyPacket>
{
public:
    ReplyPacketWriter();
    virtual ~ReplyPacketWriter() override; 
    // IDataWriter interface
public:
    int retrieveSize(const ReplyPacket &src) override;
    bool write(const ReplyPacket &src, Data &dest) override;
};

#endif // REPLYPACKETWRITER_H
