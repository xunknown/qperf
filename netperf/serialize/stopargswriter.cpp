﻿#include "stopargswriter.h"
#include <data/stopargs.h>
#include <QtEndian>
 
StopArgsWriter::StopArgsWriter()
{

}

StopArgsWriter::~StopArgsWriter()
{

}

int  StopArgsWriter::retrieveSize(const StopArgs &src)
{
    Q_UNUSED(src);
    return 24;  // 定长。
}

bool StopArgsWriter::write(const StopArgs &src, Data &dest)
{
    QByteArray destBuf; // 定义编码缓存。
    PerfResult rst =  src.result();

    // 测试次数 4字节整型。
    int testCount = rst.testCount();
    testCount = qToBigEndian<int>(testCount);
    destBuf.append(reinterpret_cast<char *>(&testCount), 4);

    // 超时次数 4字节整型。
    int timeOutCount = rst.timeOutCount();
    timeOutCount = qToBigEndian<int>(timeOutCount);
    destBuf.append(reinterpret_cast<char *>(&timeOutCount), 4);

    //平均耗时 4字节，单精度浮点。
    float average = rst.avergeElapsed();
    average = qToBigEndian<float>(average);
    destBuf.append(reinterpret_cast<char *>(&average), 4);

    // 最长耗时 4字节，单精度浮点。
    float max = rst.maxElapsed();
    max = qToBigEndian<float>(max);
    destBuf.append(reinterpret_cast<char *>(&max), 4);

    // 最短耗时 4字节，单精度浮点。
    float min = rst.minElapsed();
    min = qToBigEndian<float>(min);
    destBuf.append(reinterpret_cast<char *>(&min), 4);

    destBuf.append(4, char('\x0')); //  4字节保留。
    // 赋值输出。
    dest.setOffset(0);
    dest.setBuffer(destBuf);
    return true;
}
