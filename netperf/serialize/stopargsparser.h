﻿#ifndef STOPARGSPARSER_H
#define STOPARGSPARSER_H

#include "idataparser.h"
 
class StopArgs;
/**
 * @brief 停止参数。采用大端字节序。
 * =====================================
 * 字节序  长度（字节）  名称  备注
 * 0    4   测试次数    整型
 * 4    4   超时次数
 * 8    4  平均耗时    单精度浮点型，单位：ms
 * 12   4   最长耗时    单精度浮点型，单位：ms
 * 16   4   最短耗时    单精度浮点型，单位：ms
 * 20   4   保留
 * =====================================
 */
class StopArgsParser : public IDataParser<StopArgs>
{
public:
    StopArgsParser();
    virtual ~StopArgsParser() override;
    
    // IDataParser interface
public: 
    int parse(const Data &srcData, StopArgs &destData) override;
};
 

#endif // STOPARGSPARSER_H 
