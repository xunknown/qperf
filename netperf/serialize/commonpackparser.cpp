﻿#include "commonpackparser.h" 
#include <data/commonpack.h> 
#include <QtEndian>
 
CommonPackParser::CommonPackParser()
{

}

CommonPackParser::~CommonPackParser()
{

}

int CommonPackParser::parse(const Data &srcData, CommonPack &destData)
{
    QByteArray src = srcData.buffer();
    int index = srcData.offset();
    const char *srcArray = src.data();
    if(srcData.size() - index < 4)
    {
        return 0;
    }
    // 验证头标识。
    int header = qFromBigEndian<int>(&srcArray[index]);
    index += 4;
    if(header != HeadFlg) // 头标识验证不通过。
    {
        return -1;
    }

    if(srcData.size() - index < 2)
    {
        return 0;
    }
    // 类型。
    quint16 type = qFromBigEndian<quint16>(&srcArray[index]);
    index += 2;

    if(srcData.size() - index < 4)
    {
        return 0;
    }
    // 数据长度。
    int contentSize =  qFromBigEndian<int>(&srcArray[index]);
    index += 4;
    if(contentSize > MaxContentSize || contentSize < 0) // 数据长度超过限制。
    {
        return -2;
    }

    // 保留字段不验证。
    index += 6;

    // 当前数据不足，返回0。
    if(index + contentSize > srcData.size())
    {
        return 0;
    }

    QByteArray destContent = src.mid(index, contentSize);
    destData.setType(CommonPack::Type(type));
    destData.setContent(destContent);
    return index + contentSize;
}
