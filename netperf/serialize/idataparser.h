﻿#include "data.h"

#ifndef IDATAPARSER_H
#define IDATAPARSER_H

/**
 * 表示数据解析接口。
 */
template<typename TData>
class IDataParser
{

public:
    IDataParser()
    {
    }

    virtual ~IDataParser()
    {
    }

    /**
     * 开始解析。
     */
    virtual int parse(const Data& srcData, TData& destData)
    {
        Q_UNUSED(srcData);
        Q_UNUSED(destData);
        return -1;
    }

};
#endif // HEADER 
