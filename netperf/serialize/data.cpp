﻿#include "data.h"

Data::Data()
{
    m_offset =  0;
}

Data::Data(const QByteArray &buffer)
{
    m_buffer = buffer;
    m_offset =  0;
}

Data::Data(const char *buffer, int size)
{
    m_buffer = QByteArray(buffer, size);
    m_offset =  0;
}

Data::~Data()
{

}

/**
 * 数据地址。
 */
const char* Data::data(int &size)
{
    size = m_buffer.size();
    return m_buffer.constData();
}


/**
 * 数据地址。
 */
void Data::setData(char* data, int size)
{
    m_buffer = QByteArray(data, size);
}

int Data::size() const
{
    return m_buffer.size();
}

QByteArray Data::buffer() const
{
    return m_buffer;
}

void Data::setBuffer(const QByteArray &buffer)
{
    m_buffer = buffer;
}

int Data::offset() const
{
    return m_offset;
}

void Data::setOffset(int offset)
{
    m_offset = offset;
}

int Data::remainSize() const
{
    return size() - offset();
}
