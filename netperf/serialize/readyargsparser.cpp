﻿#include "readyargsparser.h" 
#include <data/readyargs.h>
#include <QtEndian>
ReadyArgsParser::ReadyArgsParser()
{

}

ReadyArgsParser::~ReadyArgsParser()
{

}

int ReadyArgsParser::parse(const Data &srcData, ReadyArgs &destData)
{
    QByteArray srcBuf = srcData.buffer();
    const char *pSrc = srcBuf.data();
    int index = srcData.offset();
    
    // 本地地址字段的解析。 4 字节。
    if(index + 4 > srcBuf.size()){
        return 0; // 返回0表示当前数据的长度不足。
    }
    quint32 localHost;
    localHost = qFromBigEndian<quint32>(&pSrc[index]); // 解析字段。
    destData.setLocalHost(QHostAddress(localHost)); // 赋值
    index += 4;   
    
    // 本地端口字段的解析。 2 字节。
    if(index + 2 > srcBuf.size()){
        return 0; // 返回0表示当前数据的长度不足。
    }
    quint16 localPort;
    localPort = qFromBigEndian<quint16>(&pSrc[index]); // 解析字段。
    destData.setLocalPort(localPort); // 赋值
    index += 2;   
    
    // 字段的解析。 2 字节。
    if(index + 2 > srcBuf.size()){
        return 0; // 返回0表示当前数据的长度不足。
    }
    QByteArray reserve;
    reserve.append(&pSrc[index], 2); // 解析字段。
    index += 2;   
    
    return index;
}
