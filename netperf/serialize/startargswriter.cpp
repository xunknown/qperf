﻿#include "startargswriter.h"
#include <data/startargs.h>
#include <QtEndian>
 
StartArgsWriter::StartArgsWriter()
{

}

StartArgsWriter::~StartArgsWriter()
{

}

int  StartArgsWriter::retrieveSize(const StartArgs &src)
{
    Q_UNUSED(src);
    return 32; // 固定长度。
}

bool StartArgsWriter::write(const StartArgs &src, Data &dest)
{
    //   编码逻辑。
    QByteArray buffer;
    // 发送频率 4 字节。
    int sendFreq = src.packFreq();
    qToBigEndian<int>(&sendFreq, 1, &sendFreq);
    buffer.append(reinterpret_cast<char *>(&sendFreq), 4);

    // 总包数。 4字节。
    int totalPack = src.totalCount();
    qToBigEndian<int>(&totalPack, 1, &totalPack);
    buffer.append(reinterpret_cast<char *>(&totalPack), 4);

    // 每包数据长度， 4字节。
    int perPackSize = src.perPackSize();
    qToBigEndian<int>(&perPackSize, 1, &perPackSize);
    buffer.append(reinterpret_cast<char *>(&perPackSize), 4);

    // 超时时间，4字节。
    int timeOut = src.timeOut();
    qToBigEndian<int>(&timeOut, 1, &timeOut);
    buffer.append(reinterpret_cast<char *>(&timeOut), 4);

    IPFiveTuple tuple = src.ipTuple();
    // 服务类型， 1字节。
    buffer.append(char(tuple.type()));

    // 源地址。4字节。
    quint32 srcAddr = tuple.localAddr().toIPv4Address();
    qToBigEndian<int>(&srcAddr, 1, &srcAddr);
    buffer.append(reinterpret_cast<char *>(&srcAddr), 4);

    // 目的地址。4字节。
    quint32 destAddr = tuple.remoteAddr().toIPv4Address();
    qToBigEndian<int>(&destAddr, 1, &destAddr);
    buffer.append(reinterpret_cast<char *>(&destAddr), 4);

    // 源端口 2字节。
    quint16 srcPort = tuple.localPort();
    qToBigEndian<quint16>(&srcPort, 1, &srcPort);
    buffer.append(reinterpret_cast<char *>(&srcPort), 2);

    // 目的端口 2字节。
    quint16 destPort = tuple.remotePort();
    qToBigEndian<quint16>(&destPort, 1, &destPort);
    buffer.append(reinterpret_cast<char *>(&destPort), 2);

    // 热身数据包 2字节。
    quint16 warmSize = quint16(src.warmPack());
    qToBigEndian<quint16>(&warmSize, 1, &warmSize);
    buffer.append(reinterpret_cast<char *>(&warmSize), 2);

    // 变流数据 1字节。
    buffer.append(1, char('\x0'));

    // 赋值。
    dest.setOffset(0);
    dest.setBuffer(buffer);
    return true;
}
