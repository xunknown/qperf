﻿#ifndef DATA_H
#define DATA_H

#include <QByteArray>

/**
 * 表示数据流。
 */
class Data
{
public:
    Data();
    Data(const QByteArray &buffer);
    Data(const char *buffer, int size);
    virtual ~Data();

    /**
     * 获取数据地址。
     */
    const char* data(int &size);
    void setData(char* data, int size);

    /**
     * @brief size 获取数据长度。
     * @return
     */
    int size() const;

    /**
     * @brief buffer 获取缓存。
     * @return
     */
    QByteArray buffer() const;
    void setBuffer(const QByteArray &buffer);

    /**
     * @brief offset 获取数据起始偏移量。
     * @return
     */
    int offset() const;
    void setOffset(int offset);

    /**
     * @brief remainSize 获取剩余数据长度。
     * @return 剩余长度（字节）
     */
    int remainSize() const;
private:
    QByteArray m_buffer;
    int m_offset;

};

#endif // HEADER 
