﻿#ifndef READYARGSPARSER_H
#define READYARGSPARSER_H 
#include "idataparser.h"
class ReadyArgs;
/**
 * @brief 表示准备完毕参数解析器。
 */
class ReadyArgsParser : public IDataParser<ReadyArgs>
{
public:
    ReadyArgsParser();
    virtual ~ReadyArgsParser() override;
    
    // IDataParser interface
public: 
    int parse(const Data &srcData, ReadyArgs &destData) override;
};
 

#endif // READYARGSPARSER_H 