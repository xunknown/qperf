﻿#include "commonpackwriter.h"
#include <data/commonpack.h>
#include <QtEndian>

CommonPackWriter::CommonPackWriter()
{

}

CommonPackWriter::~CommonPackWriter()
{

}

int  CommonPackWriter::retrieveSize(const CommonPack &src)
{
    // 返回实体编码后数据长度。
    int size = src.content().size() + 16; // 头固定为16字节。
    return size;
}

bool CommonPackWriter::write(const CommonPack &src, Data &dest)
{
    int contentSize = src.content().size();
    if(contentSize > MaxContentSize)
    {
        return false;
    }
    //   编码逻辑。 固定头。 4字节
    QByteArray tmp;
    int headFlag = HeadFlg;
    qToBigEndian<int>(&headFlag, 1, &headFlag);
    tmp.append(reinterpret_cast<char *>(&headFlag), 4);

    // 数据类型。 2字节
    quint16 type = src.type();
    qToBigEndian<quint16>(&type, 1, &type);
    tmp.append(reinterpret_cast<char *>(&type), 2);

    // 数据长度。 4字节。
    qToBigEndian<int>(&contentSize, 1, &contentSize);
    tmp.append(reinterpret_cast<char *>(&contentSize), 4);

    // 保留。6字节。
    tmp.append(6, char('\x0'));
    // 数据内容。
    tmp.append(src.content());

    dest.setBuffer(tmp);
    dest.setOffset(0);
    return true;
}
