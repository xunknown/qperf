﻿#include "stopargsparser.h" 
#include <data/stopargs.h> 
#include <QtEndian>

StopArgsParser::StopArgsParser()
{

}

StopArgsParser::~StopArgsParser()
{

}

int StopArgsParser::parse(const Data &srcData, StopArgs &destData)
{
    // 解析逻辑。
    if(srcData.remainSize() < 24)
    {
        return 0;
    }

    int index = srcData.offset();
    QByteArray srcBuf = srcData.buffer();
    const char *src = srcBuf.data();
    PerfResult result;

    // 测试次数 4字节。
    int testCount;
    qFromBigEndian<int>(&src[index], 1, &testCount);
    index += 4;
    result.setTestCount(testCount);

    // 超时次数 4字节。
    int timeOutCount;
    qFromBigEndian<int>(&src[index], 1, &timeOutCount);
    index += 4;
    result.setTimeOutCount(timeOutCount);

    // 平均耗时 4字节浮点。
    float average;
    qFromBigEndian<float>(&src[index], 1, &average);
    index += 4;
    result.setAvergeElapsed(average);

    // 最长耗时 4字节浮点。
    float max;
    qFromBigEndian<float>(&src[index], 1, &max);
    index += 4;
    result.setMaxElapsed(max);

    // 最短耗时 4字节浮点。
    float min;
    qFromBigEndian<float>(&src[index], 1, &min);
    index += 4;
    result.setMinElapsed(min);

    // 保留 4字节。
    index += 4;
    destData.setResult(result);
    return index;
}
