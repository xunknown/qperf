﻿#include "readyargswriter.h"
#include <data/readyargs.h>
#include <QtEndian>
ReadyArgsWriter::ReadyArgsWriter()
{

}

ReadyArgsWriter::~ReadyArgsWriter()
{

}

int  ReadyArgsWriter::retrieveSize(const ReadyArgs &src)
{
    Q_UNUSED(src);
    int packSize = 8; // 固定长度。
    return packSize; // 返回实体编码后数据长度。
}

bool ReadyArgsWriter::write(const ReadyArgs &src, Data &dest)
{
    // 编码逻辑。  
    QByteArray buffer; // 定义写入的数据buffer。
    int offset = 0; // 定义写入数据的偏移。
    
    // 本地地址字段的编码。 4 字节。
    quint32 localHost = src.localHost().toIPv4Address(); // 直接关联字段赋值。
    localHost = qToBigEndian<quint32>(localHost);// 字节序重置。 
    buffer.append(reinterpret_cast<char *>(&localHost), 4);
    offset += 4;
    
    // 本地端口字段的编码。 2 字节。
    quint16 localPort = src.localPort(); // 直接关联字段赋值。
    localPort = qToBigEndian<quint16>(localPort);// 字节序重置。 
    buffer.append(reinterpret_cast<char *>(&localPort), 2);
    offset += 2;
    
    // 字段的编码。 2 字节。
    QByteArray reserve = QByteArray(2, char(0)); // 保留字段赋值。
    buffer.append(reserve);  
    offset += 2;
    
    dest.setBuffer(buffer);
    dest.setOffset(0);
    return true;
}
