﻿#ifndef STOPARGSWRITER_H
#define STOPARGSWRITER_H

#include "idatawriter.h"
 
class StopArgs;

/**
 * @brief 表示停止参数编码器。使用大端字节序。
 * =====================================
 * 字节序  长度（字节）  名称  备注
 * 0    4   测试次数    整型
 * 4    4   超时次数
 * 8    4  平均耗时    单精度浮点型，单位：ms
 * 12   4   最长耗时    单精度浮点型，单位：ms
 * 16   4   最短耗时    单精度浮点型，单位：ms
 * 20   4   保留
 * =====================================
 */
class StopArgsWriter : public IDataWriter<StopArgs>
{
public:
    StopArgsWriter();
    virtual ~StopArgsWriter() override; 
    // IDataWriter interface
public:
    int retrieveSize(const StopArgs &src) override;
    bool write(const StopArgs &src, Data &dest) override;
};

#endif // STOPARGSWRITER_H
