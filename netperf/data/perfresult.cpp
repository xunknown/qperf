﻿#include "perfresult.h"

PerfResult::PerfResult()
{

}

PerfResult::PerfResult(int testCount, int timeOutCount, float avergeElapsed, float maxElapsed, float minElapsed)
{
    m_testCount = testCount;
    m_timeOutCount = timeOutCount;
    m_avergeElapsed = avergeElapsed;
    m_maxElapsed = maxElapsed;
    m_minElapsed = minElapsed;
}

int PerfResult::testCount() const
{
    return m_testCount;
}

void PerfResult::setTestCount(const int &testCount)
{
    m_testCount = testCount;
}

int PerfResult::timeOutCount() const
{
    return m_timeOutCount;
}

void PerfResult::setTimeOutCount(int timeOutCount)
{
    m_timeOutCount = timeOutCount;
}

float PerfResult::avergeElapsed() const
{
    return m_avergeElapsed;
}

void PerfResult::setAvergeElapsed(float avergeElapsed)
{
    m_avergeElapsed = avergeElapsed;
}

float PerfResult::maxElapsed() const
{
    return m_maxElapsed;
}

void PerfResult::setMaxElapsed(float maxTime)
{
    m_maxElapsed = maxTime;
}

float PerfResult::minElapsed() const
{
    return m_minElapsed;
}


void PerfResult::setMinElapsed(float minTime)
{
    m_minElapsed = minTime;
}
