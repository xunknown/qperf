#include "replypacket.h"

ReplyPacket::ReplyPacket()
{
    m_flag = 0;
}

int ReplyPacket::flag() const
{
    return m_flag;
}

void ReplyPacket::setFlag(int flag)
{
    m_flag = flag;
}

QByteArray ReplyPacket::content() const
{
    return m_content;
}

void ReplyPacket::setContent(const QByteArray &content)
{
    m_content = content;
}
