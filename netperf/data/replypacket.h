﻿#ifndef REPLYPACKET_H
#define REPLYPACKET_H

#include <QByteArray>

/**
 * @brief The ReplyPacket class  回复数据包。
 */
class ReplyPacket
{
public:
    ReplyPacket();

    /**
     * @brief flag 获取唯一标识。
     * @return 返回唯一标识。
     */
    int flag() const;
    /**
     * @brief setFlag 设置唯一标识。
     * @param flag 唯一标识。
     */
    void setFlag(int flag);

    /**
     * @brief content 获取数据内容。
     * @return 返回数据内容。
     */
    QByteArray content() const;
    /**
     * @brief setContent 设置数据内容。
     * @param content 数据内容。
     */
    void setContent(const QByteArray &content);

private:
    int m_flag;
    QByteArray m_content;
};

#endif // REPLYPACKET_H
