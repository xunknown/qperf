﻿#ifndef READYARGS_H
#define READYARGS_H  
#include <QHostAddress>
/**
* @brief 表示准备完毕参数。
*/
class ReadyArgs
{
public:
    ReadyArgs();
    /**
     * @brief ReadyArgs 准备完毕参数。
     * @param localHost 本地地址。
     * @param localPort 本地端口。
     */
    ReadyArgs(const QHostAddress &localHost, quint16 localPort);
    virtual ~ReadyArgs();
public:
    /**
    * @biref  获取本地地址。
    * @return 返回本地地址。
    **/
    QHostAddress localHost() const;
    /**
    * @biref  设置本地地址。
    * @param  localHost 设置的本地地址。
    **/
    void setLocalHost(const QHostAddress &localHost);
        
    /**
    * @biref  获取本地端口。
    * @return 返回本地端口。
    **/
    quint16 localPort() const;
    /**
    * @biref  设置本地端口。
    * @param  localPort 设置的本地端口。
    **/
    void setLocalPort(const quint16 &localPort);
        
private: 
    QHostAddress m_localHost; // 本地地址 。
    quint16 m_localPort; // 本地端口 。
};

#endif // READYARGS_H
