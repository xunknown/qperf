﻿#ifndef COMMONPACK_H
#define COMMONPACK_H 

#include <QByteArray>


/**
* @brief 表示通用数据封包。
*/
class CommonPack
{
public:
    /**
     * @brief The Type enum 定义数据包类型。
     */
    enum Type
    {
        // 启动。
        Type_Start,
        // 停止。
        Type_Stop,
        // 请求。
        Type_Request,
        // 响应。
        Type_Reply,
        // 就绪
        Type_Ready
    };
public:
    CommonPack();
    CommonPack(CommonPack::Type type, QByteArray content);
    virtual ~CommonPack(); 

    /**
     * @brief type 获取当前数据包类型。
     * @return
     */
    CommonPack::Type type() const;
    /**
     * @brief setType 设置当前数据包类型。
     * @param type
     */
    void setType(const CommonPack::Type &type);

    /**
     * @brief content 获取数据内容。
     * @return 返回数据内容。
     */
    QByteArray content() const;
    /**
     * @brief setContent 设置数据内容。
     * @param content
     */
    void setContent(const QByteArray &content);

private:
    CommonPack::Type m_type; // 数据包类型。
    QByteArray m_content; // 数据内容。

};

#endif // COMMONPACK_H
