﻿#ifndef PERFRESULT_H
#define PERFRESULT_H

/**
 * @brief The PerfResult class
 *  表示测试结果实体。
 */
class PerfResult
{
public:
    PerfResult();
    /**
     * @brief StopArgs
     * @param testCount
     * @param timeOutCount
     * @param avergeElapsed
     * @param maxElapsed
     * @param minElapsed
     */
    PerfResult(int testCount, int timeOutCount, float avergeElapsed, float maxElapsed, float minElapsed);
    /**
     * @brief testCount 测试次数。
     * @return 返回测试次数。
     */
    int testCount() const;
    /**
     * @brief setTestCount 设置测试次数。
     * @param testCount 测试次数。
     */
    void setTestCount(const int &testCount);

    /**
     * @brief timeOutCount 获取超时次数。
     * @return 返回超时次数。
     */
    int timeOutCount() const;
    /**
     * @brief setTimeOutCount 设置超时次数。
     * @param timeOutCount 超时次数。
     */
    void setTimeOutCount(int timeOutCount);

    /**
     * @brief avergeElapsed 获取平均时长（毫秒）。
     * @return
     */
    float avergeElapsed() const;
    /**
     * @brief setAvergeElapsed  设置平均时长。
     * @param avergeElapsed
     */
    void setAvergeElapsed(float avergeElapsed);

    /**
     * @brief maxElapsed 获取最大时长（毫秒）。
     * @return 返回最大时长。
     */
    float maxElapsed() const;
    /**
     * @brief setMaxElapsed 设置最大时长（毫秒）。
     * @param maxElapsed
     */
    void setMaxElapsed(float maxElapsed);

    /**
     * @brief minElapsed 获取最小时长（毫秒）。
     * @return 返回最小时长。
     */
    float minElapsed() const;
    /**
     * @brief setMinElapsed 设置最小时长。
     * @param minElapsed 最小时长。
     */
    void setMinElapsed(float minElapsed);

private:
    int m_testCount; // 测试次数。
    int m_timeOutCount;  //  超时次数。
    float m_avergeElapsed; // 平均时长（毫秒）。
    float m_maxElapsed; // 最大时长（毫秒）。
    float m_minElapsed; // 最小时长（毫秒）。
};

#endif // PERFRESULT_H
