﻿#include "requestpacket.h"

RequestPacket::RequestPacket()
{
    m_flag = 0;
}

int RequestPacket::flag() const
{
    return m_flag;
}

void RequestPacket::setFlag(int flag)
{
    m_flag = flag;
}

QByteArray RequestPacket::content() const
{
    return m_content;
}

void RequestPacket::setContent(const QByteArray &content)
{
    m_content = content;
}
