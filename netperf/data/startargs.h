﻿#ifndef STARTARGS_H
#define STARTARGS_H

#include <ipfivetuple.h>

/**
 * @brief The StartArgs class
 * 表示启动的参数。
 */
class StartArgs
{
public:
    StartArgs();
    /**
     * @brief packFreq 获取测试发包频率（包/s）
     * @return
     */
    int packFreq() const;
    /**
     * @brief setPackFreq 设置发包频率（包/s）
     * @param packFreq
     */
    void setPackFreq(int packFreq);

    /**
     * @brief totalCount 获取测试总包数。
     * @return
     */
    int totalCount() const;
    /**
     * @brief setTotalCount 设置测试总包数。
     * @param totalCount 总包数。
     */
    void setTotalCount(int totalCount);

    /**
     * @brief perPackSize  获取每包数据长度（字节）。
     * @return
     */
    int perPackSize() const;
    /**
     * @brief setPerPackSize 设置每包数据长度（字节）。
     * @param perPackSize
     */
    void setPerPackSize(int perPackSize);

    /**
     * @brief timeOut  获取超时时间（ms）
     * @return
     */
    int timeOut() const;
    /**
     * @brief setTimeOut 设置超时时间（ms）
     * @param timeOut
     */
    void setTimeOut(int timeOut);

    /**
     * @brief ipTuple 获取ip链路信息。
     * @return 返回链路信息。
     */
    IPFiveTuple ipTuple() const;
    /**
     * @brief setIpTuple 设置ip链路信息。
     * @param ipTuple
     */
    void setIpTuple(const IPFiveTuple &ipTuple);
    /**
     * @brief warmPack 热身包数。
     * @return 返回热身包个数据。
     */
    int warmPack() const;
    /**
     * @brief setWarmPack 设置热身包数。
     * @param warmPack 设置热身包数。
     */
    void setWarmPack(int warmPack);

    /**
     * @brief toString 格式化数据。
     * @return 返回格式化数据。
     */
    QString toString();
private:
    int m_perPackSize; // 每包字节长度。
    int m_totalCount; // 总数据包长度。
    int m_packFreq; // 测试频率（包/s）。
    int m_timeOut; // 超时长度ms。
    int m_warmPack; // 热身数据包个数。
    IPFiveTuple m_ipTuple; // 数据链路地址。

};

#endif // STARTARGS_H
