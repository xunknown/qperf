﻿#include "commonpack.h" 
 
 
CommonPack::CommonPack()
{

}

CommonPack::CommonPack(CommonPack::Type type, QByteArray content)
{
    m_type = type;
    m_content = content;
}

CommonPack::~CommonPack()
{

}

CommonPack::Type CommonPack::type() const
{
    return m_type;
}

void CommonPack::setType(const CommonPack::Type &type)
{
    m_type = type;
}

QByteArray CommonPack::content() const
{
    return m_content;
}

void CommonPack::setContent(const QByteArray &content)
{
    m_content = content;
}
