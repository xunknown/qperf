﻿#ifndef REQUESTPACKET_H
#define REQUESTPACKET_H

#include <QByteArray>

/**
 * @brief The TestPacket class
 *  表示请求包实体。
 */
class RequestPacket
{
public:
    RequestPacket();
    /**
     * @brief flag 获取数据包的唯一标识。
     * @return 返回唯一标识。
     */
    int flag() const;
    /**
     * @brief setFlag 设置数据包唯一标识。
     * @param flag 唯一标识。
     */
    void setFlag(int flag);

    /**
     * @brief content 获取数据包内容。
     * @return 数据包内容。
     */
    QByteArray content() const;
    /**
     * @brief setContent 设置数据包内容。
     * @param content 数据包内容。
     */
    void setContent(const QByteArray &content);

private:
    int m_flag; // 唯一标识。
    QByteArray m_content; // 数据包内容。
};

#endif // TESTPACKET_H
