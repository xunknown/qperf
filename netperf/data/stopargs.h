﻿#ifndef STOPARGS_H
#define STOPARGS_H
#include "perfresult.h"

#include <QtGlobal>
/**
 * @brief The StopArgs class
 * 表示停止的参数。
 */
class StopArgs
{
public:
    StopArgs();
    /**
     * @brief result 测试结果。
     * @return
     */
    PerfResult result() const;
    /**
     * @brief setResult 设置测试结果。
     * @param result
     */
    void setResult(const PerfResult &result);

    QString toString() const;
private:
    PerfResult m_result;
};

#endif // STOPPARAMTERS_H
