﻿#include "initclientstate.h"
#include <context/context.h>
#include <pointer/basepointer.h>

InitClientState::InitClientState(Context *context, bool bServer)
    :BaseState (context, bServer)
{
}

InitClientState::~InitClientState()
{
   m_context->remove(this);
}

void InitClientState::process()
{
    m_context->add(this);
}

void InitClientState::cancel()
{
    change(BaseState::Final);
}

void InitClientState::onStateChanged(PointerCallback::State state, bool bCmd)
{
    if(bCmd)
    {
        if(state == PointerCallback::Connected)
        {
            change(BaseState::StartClient);
        }else {
            cancel();
        }
    }
}

void InitClientState::onStart(const StartArgs &args)
{
    Q_UNUSED(args)
}

void InitClientState::onStop(const StopArgs &args)
{
    Q_UNUSED(args)
}

void InitClientState::onRequest(const RequestPacket &pack)
{
    Q_UNUSED(pack)
}

void InitClientState::onReply(const ReplyPacket &pack)
{
    Q_UNUSED(pack)
}

void InitClientState::onReady(const ReadyArgs &args)
{
    Q_UNUSED(args)
}
