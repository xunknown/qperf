﻿#include "workreplystate.h"

#include <context/context.h>
#include <pointer/basepointer.h>
#include <data/replypacket.h>
#include <data/requestpacket.h>
#include <data/stopargs.h>
#include <QsLog.h>

WorkReplyState::WorkReplyState(Context *context, bool bServer)
    :BaseState (context, bServer)
{
}

WorkReplyState::~WorkReplyState()
{
    m_context->remove(this);
}

void WorkReplyState::process()
{
    m_context->add(this);
}

void WorkReplyState::cancel()
{
    change(BaseState::Stop);
}

void WorkReplyState::onStateChanged(PointerCallback::State state, bool bCmd)
{
    Q_UNUSED(bCmd) // 无论是哪种链路断开，都终止。
    if(state != PointerCallback::Connected)
    {
        cancel();
    }
}

void WorkReplyState::onStart(const StartArgs &args)
{
    Q_UNUSED(args)
}

void WorkReplyState::onStop(const StopArgs &args)
{
    Q_UNUSED(args) //  显示结果。
    QLOG_INFO() << QObject::tr("Receive StopArgs %1").arg(args.toString());
    cancel();
}

void WorkReplyState::onRequest(const RequestPacket &pack)
{
    int flg = pack.flag();
    QByteArray content = pack.content();
    ReplyPacket reply; // 构造响应包。
    reply.setFlag(flg); // 设置响应的回复。
    reply.setContent(content); // 设置内容。
    m_context->reply(reply);
}

void WorkReplyState::onReply(const ReplyPacket &pack)
{
    // 非法提示。
    QLOG_TRACE() << QObject::tr("ReplyState receive ReplyPacket, but it's not expect!");
    Q_UNUSED(pack)
}

void WorkReplyState::onReady(const ReadyArgs &args)
{
    Q_UNUSED(args)
}
