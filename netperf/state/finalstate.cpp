﻿#include "initserverstate.h"
#include "finalstate.h"
#include <tunnel/basetunnel.h>
#include <context/context.h>
#include <QsLog.h>

FinalState::FinalState(Context *context, bool bServer)
    :BaseState (context, bServer)
{
}

void FinalState::process()
{
    QLOG_TRACE() << QObject::tr(" FinalState Process!");
    m_context->changeState(Q_NULLPTR);
}

void FinalState::cancel()
{
    QLOG_TRACE() << QObject::tr(" FinalState Cancel!");
    m_context->changeState(Q_NULLPTR);
}
