﻿#ifndef INITCLIENTSTATE_H
#define INITCLIENTSTATE_H

#include "basestate.h"

#include <pointer/pointercallback.h>

/**
 * @brief The InitClientState class
 *   表示客户端初始化状态。
 */
class InitClientState : public BaseState,
        public PointerCallback
{
public:
    InitClientState(Context *context, bool bServer);
    ~InitClientState() override;
    // BaseState interface
public:
    void process() override;
    void cancel() override;

    // PointerCallback interface
public:
    void onStateChanged(PointerCallback::State state, bool bCmd) override;
    void onStart(const StartArgs &args) override;
    void onStop(const StopArgs &args) override;
    void onRequest(const RequestPacket &pack) override;
    void onReply(const ReplyPacket &pack) override;
    void onReady(const ReadyArgs &args) override;
};

#endif // INITCLIENTSTATE_H
