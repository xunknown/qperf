﻿#include "workrequeststate.h"

#include <QThread>
#include <pointer/basepointer.h>
#include <ratecontrol.h>

#include <context/context.h>

#include <data/replypacket.h>
#include <data/requestpacket.h>
#include <data/startargs.h>
#include <data/stopargs.h>
#include <QsLog.h>

WorkRequestState::WorkRequestState(Context *context, bool bServer)
    :BaseState(context, bServer)
{
    m_workThread = Q_NULLPTR;
}

WorkRequestState::~WorkRequestState()
{
    if(m_workThread && m_workThread->isRunning()
            && m_workThread != QThread::currentThread())
    {
        m_workThread->requestInterruption();
        m_workThread->wait(); // 等待线程释放。
    }
    m_context->remove(this);

    foreach(auto item, m_records)
    {
        delete item;
    }
    m_records.clear();
}

void WorkRequestState::process()
{
    m_context->add(this);

    m_workThread = QThread::create([&]{this->runWorker();});
    m_workThread->start();
}

void WorkRequestState::cancel()
{
    change(BaseState::Stop);
}

void WorkRequestState::onStateChanged(PointerCallback::State state, bool bCmd)
{
    Q_UNUSED(bCmd)
    if(state != PointerCallback::Connected)
    {
        cancel();
    }
}

void WorkRequestState::onStart(const StartArgs &args)
{
    Q_UNUSED(args)
}

void WorkRequestState::onStop(const StopArgs &args)
{
    Q_UNUSED(args)
    cancel();
}

void WorkRequestState::onRequest(const RequestPacket &pack)
{
    Q_UNUSED(pack)
    //  非法提示。
    QLOG_WARN() << QObject::tr("WorkRequestState reveice RequestPacket, it's not expect!");
}

void WorkRequestState::onReply(const ReplyPacket &pack)
{
   // 接收到响应包，停止计时
    int flag = pack.flag();
    int currentSize = m_records.size();
    if(flag >= currentSize || flag < 0)
    {
        // 打印日志，不是期望的数据包。
        QString msg = QString(QObject::tr("Receive request unexpect reply flag :%1, current record size:%2:%3."))
                .arg(flag).arg(currentSize).arg(m_records.size());
        QLOG_WARN() << msg;
        return ;
    }
    m_records[flag]->stop();
}

void WorkRequestState::onReady(const ReadyArgs &args)
{
    Q_UNUSED(args)
}

void WorkRequestState::resultCompute(int timeOut, int warmCount, StopArgs &args)
{
    int testCnt = m_records.size();
    int timeOutCnt = 0;
    float maxTime = 0;
    float minTime = timeOut;
    float totalElapsed = 0;
    int warmCounter = warmCount;
    foreach(auto item, m_records)
    {
        if(warmCounter > 0) // 跳过热身数据包。
        {
            warmCounter--;
            continue;
        }
        float tmpElapsed = item->elapsed();
        if(tmpElapsed > timeOut)
        {
            timeOutCnt++;
        }else {
            totalElapsed += quint64(tmpElapsed);
            if(maxTime < tmpElapsed)
            {
                maxTime = tmpElapsed;
            }
            if(minTime > tmpElapsed)
            {
                minTime  = tmpElapsed;
            }
        }
    }
    float avergeElaped = totalElapsed/float(testCnt-timeOutCnt); // 超时数据包不计入平均。
    PerfResult result(testCnt - warmCount, timeOutCnt, avergeElaped, maxTime, minTime);
    args.setResult(result);
}

bool WorkRequestState::request(RateControl &rateCtrl, RequestPacket &pack)
{
    Record *tmpRcd = new Record(pack.flag());
    m_records.append(tmpRcd);
    tmpRcd->start(); // 计时开始后请求。

    bool req = m_context->request(pack);
    if(!req)
    {
        return false;
    }

    int waitTime = rateCtrl.addSample(1);
    if(m_workThread->isInterruptionRequested())
    {
        return false;
    }
    if(waitTime > 0 )
    {
        QThread::msleep(uint(waitTime));
    }
    return true;
}

void WorkRequestState::workComplete(StartArgs *startArgs)
{
    int timeout = startArgs->timeOut();
    int warmCount = startArgs->warmPack();
    QThread::msleep(uint(timeout)); // 等待未处理完成的。

    foreach(auto item, m_records) // 所有停止计数。
    {
        item->stop();
    }
    // 执行完毕。 开始计算结果
    StopArgs stopArgs;
    resultCompute(timeout, warmCount, stopArgs);
    m_context->stop(stopArgs);  // 通知外部显示、处理结果。
    if(m_workThread->isInterruptionRequested())
    {
       return;
    }
    change(BaseState::Stop);
}

void WorkRequestState::runRequest(StartArgs *startArgs)
{
    int sampling = startArgs->packFreq();// 每秒的数据包发送次数。
    int totalReqCount = startArgs->totalCount(); // 总请求次数。通过start参数赋值。
    int timeOut = startArgs->timeOut(); // 超时时间。 毫秒。
    int reqSize = startArgs->perPackSize(); // 每包数据长度。
    int warmCount = startArgs->warmPack(); // 热身数据包。

    QLOG_INFO() << QString(QObject::tr("Run request:%1; sampling:%2; requestCount:%3; timeOut(ms):%4, warm packet:%5."))
                   .arg(reqSize)
                   .arg(sampling)
                   .arg(totalReqCount)
                   .arg(timeOut)
                   .arg(warmCount);

    RateControl ctrl(sampling);
    ctrl.start();

    RequestPacket pack;
    QByteArray reqContent(reqSize, char(0x88));
    pack.setContent(reqContent);

    totalReqCount += warmCount; // 总包数 = 热身包 + 请求总包数。
    m_records.reserve(totalReqCount);
    for (int i = 0; i < totalReqCount; i++) {
        pack.setFlag(i);
        if(i >= warmCount)
        {
            m_context->setProcPercent(float(i - warmCount) / float(totalReqCount - warmCount));
        }
        if(!request(ctrl, pack)){
            break;
        }
    }
}

void WorkRequestState::runWorker()
{
    //  性能测试主要逻辑。
    StartArgs *startArgs = m_context->startArgs();

    runRequest(startArgs); // 执行请求。

    workComplete(startArgs);// 统计结果。
}

WorkRequestState::Record:: Record(int flags)
{
    m_flags = flags;
    m_time = new QTime;
    m_elaped = -1.0f;
    m_stopped = false;
}

WorkRequestState::Record::~Record()
{
    delete m_time;
}

void WorkRequestState::Record::start()
{
    m_time->start();
}

void WorkRequestState::Record::stop()
{
    if(!m_stopped)
    {
        m_stopped = true;
        m_elaped = m_time->elapsed();
    }
}

float WorkRequestState::Record::elapsed()
{
    return m_elaped;
}
