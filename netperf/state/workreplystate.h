﻿#ifndef WORKREPLYSTATE_H
#define WORKREPLYSTATE_H

#include "basestate.h"

#include <pointer/pointercallback.h>

/**
 * @brief The WorkReplyState class
 *  表示回复工作状态。
 */
class WorkReplyState : public BaseState,
        public PointerCallback
{
public:
    WorkReplyState(Context *context, bool bServer);
    ~WorkReplyState() override;
    // PerfState interface
public:
    void process() override;
    void cancel() override;

    // PointerCallback interface
public:
    void onStateChanged(PointerCallback::State state, bool bCmd) override;
    void onStart(const StartArgs &args) override;
    void onStop(const StopArgs &args) override;
    void onRequest(const RequestPacket &pack) override;
    void onReply(const ReplyPacket &pack) override;
    void onReady(const ReadyArgs &args) override;
};

#endif // WORKREPLYSTATE_H
