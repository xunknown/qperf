﻿#ifndef STARTSERVERSTATE_H
#define STARTSERVERSTATE_H

#include "basestate.h"

#include <pointer/pointercallback.h>


/**
 * @brief The StartServerState class
 *  启动中状态。
 */
class StartServerState : public BaseState,
        public PointerCallback
{
public:
    StartServerState(Context *context, bool bServer);
    ~StartServerState() override;
    // PerfState interface
public:
    void process() override;
    void cancel() override;

    // PointerCallback interface
public:
    void onStateChanged(PointerCallback::State state, bool bCmd) override;
    void onStart(const StartArgs &args) override;
    void onStop(const StopArgs &args) override;
    void onRequest(const RequestPacket &pack) override;
    void onReply(const ReplyPacket &pack) override;
    void onReady(const ReadyArgs &args) override;
};

#endif // STARTSERVERSTATE_H
