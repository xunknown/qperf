﻿#ifndef  FINALSTATE_H
#define  FINALSTATE_H

#include "basestate.h"

/**
 * @brief The FinalState class
 * 表示未初始的状态。
 */
class FinalState : public BaseState
{
public:
    /**
     * @brief UnInitState  未初始的状态。
     * @param context 上下文实例。
     * @param bServer 是否服务端。
     */
    FinalState(Context *context, bool bServer);

    // PerfState interface
public:
    void process() override;
    void cancel() override;
};

#endif // FINALSTATE_H
