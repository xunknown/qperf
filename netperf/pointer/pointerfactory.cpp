﻿#include "fakeclientpointer.h"
#include "fakeserverpointer.h"
#include "pfpointer.h"
#include "pointerfactory.h"

PointerFactory::PointerFactory(bool bServer, bool bFake)
{
    m_bServer = bServer;
    m_bFake = bFake;
}

BasePointer *PointerFactory::create()
{
    if(m_bFake)
    {
        if(m_bServer)
            return new FakeServerPointer;
        return new FakeClientPointer;
    }else
    {
        return new PFPointer;
    }
}
