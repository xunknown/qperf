﻿#ifndef POINTERFACTORY_H
#define POINTERFACTORY_H

class BasePointer;
/**
 * @brief The PointerFactory class
 * 远端节点抽象类的实现 。
 */
class PointerFactory
{
public:
    /**
     * @brief PointerFactory
     * @param bServer 是否服务端。
     * @param bFake 是否虚拟实现。
     */
    PointerFactory(bool bServer, bool bFake = false);
    BasePointer *create();
private:
    bool m_bServer; // 是否服务端。
    bool m_bFake; // 是否虚拟实现。
};

#endif // POINTERFACTORY_H
