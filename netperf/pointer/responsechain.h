﻿#ifndef RESPONSECHAIN_H
#define RESPONSECHAIN_H

#include <QSharedPointer>
#include <functional>
class CommonPack;

/**
 * @brief The ResponseChain class 表示责任链类。
 */
class ResponseChain
{
public:
    ResponseChain(int type, std::function<void(QByteArray)> proceCall);
    QSharedPointer<ResponseChain> next() const;
    void setNext(const QSharedPointer<ResponseChain> &next);
    /**
     * @brief handle 处理数据包。
     * @param srcData
     * @return
     */
    bool handle(const CommonPack &srcData);
private:
    QSharedPointer<ResponseChain> m_next; // 下一个处理链实例。
    int m_type; // 处理的指令类型。
    std::function<void(QByteArray)> m_proceCall;//  处理回调。
};

#endif // RESPONSECHAIN_H
