﻿#ifndef CHAINMANAGE_H
#define CHAINMANAGE_H

#include <QSharedPointer>


class ResponseChain;
class CommonPack;
/**
 * @brief The ChainManage class 责任链管理类。
 */
class ChainManage
{
public:
    ChainManage();
    /**
     * @brief add 添加链。
     * @param chain
     */
    void add(QSharedPointer<ResponseChain> chain);
    /**
     * @brief handle 处理数据包。
     * @param srcData
     * @return
     */
    bool handle(const CommonPack &srcData);
private:
    QSharedPointer<ResponseChain> m_headerChain; // 链头，责任链的查找位置。
    QSharedPointer<ResponseChain> m_currentChain; // 当前链位置。
};

#endif // CHAINMANAGE_H
