﻿#include "integrationtest.h"
#include <ctrl/basectrl.h>
#include <ctrl/ctrlfactory.h>
#include <ctrl/ctrlfactoryproducer.h>
#include "commandparser.h"
#include "qconsoledest.h"
#include <QCommandLineParser>
#include <QCoreApplication>
#include <QDir>
#include <QsLog.h>
#include <data/initargs.h>
#include <data/startargs.h>
using namespace QsLogging;
/**
 * @brief initLog 初始日志。
 */
void initLog()
{
    Logger &logger = Logger::instance();
#ifdef DEBUG
    logger.setLoggingLevel(QsLogging::Level::TraceLevel);
#else
    logger.setLoggingLevel(QsLogging::Level::InfoLevel);
#endif
    //添加文件作为目的地
    QString logPath = QCoreApplication::applicationDirPath() + "//";
    logPath.append("Log");
    QDir dir = QDir(logPath);
    if(!dir.exists()) {
        dir.mkdir(logPath);
    }
    logPath.append("//log.txt");

    DestinationPtr fileDest(DestinationFactory::MakeFileDestination(
                                       logPath,
                                       EnableLogRotation,
                                       MaxSizeBytes(1000*1000*10),
                                       MaxOldLogCount(8)));
    logger.addDestination(fileDest);
    DestinationPtr debugDest(new QConsoleDest);
    logger.addDestination(debugDest);
}
/**
 * @brief readCmd 读取输入参数。
 * @param cmdParser
 * @param bServer
 * @param bFake
 * @param initArgs
 * @param startArgs
 */
void readCmd(CommandParser &cmdParser, bool &bServer, bool &bFake, InitArgs &initArgs, StartArgs &startArgs)
{
    bServer = cmdParser.bServer(); // 是否服务端。
    bFake = cmdParser.bFake(); // 是否启动虚拟的类。

    IPFiveTuple cmdTuple;
    bool parsed = cmdParser.cmdIPTuple(cmdTuple);
    if(!parsed)
    {
        cmdParser.showHelp();
    }

    // 初始参数。
    initArgs.setTuple(cmdTuple);
    //StartArgs startArgs;
    // Client 调用startArgs发送启动的指令。
    if(!bFake && !bServer && !cmdParser.startArgs(startArgs))
    {
        cmdParser.showHelp();
    }

    if(bServer)
    {
        QLOG_INFO() << QString(QObject::tr("Init Server. Command arguments %1")).arg(cmdTuple.toString()) ;
    }else
    {
        QLOG_INFO() << QString(QObject::tr("Init Client. Command arguments %1, start arguments %2"))
                       .arg(cmdTuple.toString())
                       .arg(startArgs.toString());
    }
}

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    initLog(); // 初始日志。

    CommandParser cmdParser;
    cmdParser.process(app);

    bool bServer; // 是否服务端。
    bool bFake; // 是否启动虚拟的类。
    StartArgs startArgs;
    InitArgs initArgs;
    readCmd(cmdParser, bServer, bFake, initArgs, startArgs);

    // 启动。
    CtrlFactoryProducer *producer = new CtrlFactoryProducer(bServer, bFake, startArgs);
    AbstractCtrlFactory *absCtrlFactory = producer->create(); // 创建抽象工厂。
    delete producer;

    // 创建控制器对象。
    BaseCtrl *ctrl = absCtrlFactory->create(initArgs);
    delete absCtrlFactory;

    if(!ctrl->start())  // 启动。
    {
        QLOG_ERROR() << QObject::tr("Init Fail!");
    }

    int ret =  app.exec();
    ctrl->deleteLater();

    return ret;
}
