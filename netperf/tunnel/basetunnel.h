﻿#ifndef BASETUNNEL_H
#define BASETUNNEL_H

#include <QVector>
#include <threadsafeset.h>

class QByteArray ;

class TunnelCallback;
class QMutex;
class IPFiveTuple;
/**
 * @brief The BaseTunnel class
 * 表示传输隧道的抽象类。
 * TODO 将远端和本地的终结点信息加入抽象类，成为抽象类的属性。
 */
class BaseTunnel
{
public:
    BaseTunnel();
    virtual ~BaseTunnel();
    /**
     * @brief add 添加回调。
     * @param item 回调实例。
     */
    void add(TunnelCallback *item);
    /**
     * @brief remove 移除回调。
     * @param item 实例。
     */
    void remove(TunnelCallback *item);
public:
    /**
     * @brief build 建立隧道。
     * @return 成功返回true。
     */
    virtual bool build() = 0;
    /**
     * @brief destory 销毁隧道。
     */
    virtual void destory() = 0;
    /**
     * @brief write 写入数据到隧道。
     * @param src 写入的数据。
     * @return 返回写入数据的长度。
     */
    virtual int write(QByteArray &src) = 0;
    /**
     * @brief state 返回传输隧道状态。
     * @return 正常返回true。
     */
    virtual bool state() = 0;

    /**
     * @brief disConnect 关闭连接。
     */
    virtual void disConnect() = 0;
    /**
     * @brief fetchTuple 获取通道代表的ip五元组信息。
     * @param tuple 输出参数。
     */
    virtual void fetchTuple(IPFiveTuple &tuple) = 0;
protected:
    /**
     * @brief 为回调集合执行方法。
     */
    template<typename DoSth>
    void execEach(DoSth &func)
    {
        m_callbacks.execEach(func);
    }
protected:
    ThreadSafeSet<TunnelCallback> m_callbacks;
};

#endif // BASETUNNEL_H
