﻿#ifndef TUNNELFACTORY_H
#define TUNNELFACTORY_H

#include <ipfivetuple.h>


class BaseTunnel;
/**
 * @brief The TunnelFactory class
 * 表示隧道工厂。
 */
class TunnelFactory
{

public:
    TunnelFactory(const IPFiveTuple &tuple);

    /**
     * @brief create 创建隧道实例。
     * @return
     */
    BaseTunnel *create();
private:
    IPFiveTuple m_tuple;
};

#endif // TUNNELFACTORY_H
