﻿#include "basetunnel.h"


BaseTunnel::BaseTunnel()
{
}

BaseTunnel::~BaseTunnel()
{
    m_callbacks.removeAll();
}

void BaseTunnel::add(TunnelCallback *item)
{
    m_callbacks.add(item);
}

void BaseTunnel::remove(TunnelCallback *item)
{
    m_callbacks.remove(item);
}

