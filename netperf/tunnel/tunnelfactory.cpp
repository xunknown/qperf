﻿#include "tcpclienttunnel.h"
#include "tcpservertunnel.h"
#include "tunnelfactory.h"
#include "udptunnel.h"

#include <ipfivetuple.h>

TunnelFactory::TunnelFactory(const IPFiveTuple &tuple)
    : m_tuple(tuple)
{

}

BaseTunnel *TunnelFactory::create()
{
    IPFiveTuple::Type type = m_tuple.type();
    BaseTunnel *tunnel = Q_NULLPTR;
    switch (type) {
    case IPFiveTuple::Type_Udp:
        tunnel = new UdpTunnel(m_tuple);
        break;
    case IPFiveTuple::Type_TcpServer:
        tunnel = new TcpServerTunnel(m_tuple);
        break;
    case IPFiveTuple::Type_TcpClient:
        tunnel = new TcpClientTunnel(m_tuple);
        break;
    }
    return tunnel;
}
