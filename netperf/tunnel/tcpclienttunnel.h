﻿#ifndef TCPCLIENTTUNNEL_H
#define TCPCLIENTTUNNEL_H

#include "basetunnel.h"

#include <net/netreading.h>
#include <net/tcpsession.h>

#include <ipfivetuple.h>
class QThread;

QNET_USING_NAMESPACE
QNET_NAMESPACE_BEGIN
class TcpClient;
QNET_NAMESPACE_END
/**
 * @brief The TcpClientTunnel class
 *  tcp客户端隧道。
 */
class TcpClientTunnel : public BaseTunnel,
        public ISessionListener,
        public NetReading
{
public:
    TcpClientTunnel(const IPFiveTuple &ipTuple);
    ~TcpClientTunnel() override;
    // BaseTunnel interface
public:
    bool build() override;
    void destory() override;
    int write(QByteArray &src) override;
    bool state() override;
    void disConnect() override;
    void fetchTuple(IPFiveTuple &tuple) override;

    // ISessionListener interface
public:
    void onStateChanged(TcpSession *session, bool connected) override;
    void onError(QAbstractSocket::SocketError error) override;
    // NetReading interface
public:
    void onProc(const QByteArray &arry) override;
private:
    QtSocket::TcpClient *m_client;
    TcpSession *m_session;
    IPFiveTuple m_ipTuple;
    bool m_state;
    QThread *m_innerThread; // 单独线程。
};

#endif // TCPCLIENTTUNNEL_H
