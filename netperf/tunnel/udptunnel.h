﻿#ifndef UDPTUNNEL_H
#define UDPTUNNEL_H

#include "basetunnel.h"

#include <net/netreading.h>
#include <ipfivetuple.h>
QNET_USING_NAMESPACE
QNET_NAMESPACE_BEGIN
class UdpChannel;
QNET_NAMESPACE_END
/**
 * @brief The UdpTunnel class
 * 表示udp通道。
 */
class UdpTunnel : public BaseTunnel,
        public NetReading
{
public:
    UdpTunnel(const IPFiveTuple &ipTuple);
    ~UdpTunnel() override;
    // BaseTunnel interface
public:
    bool build() override;
    void destory() override;
    int write(QByteArray &src) override;
    bool state() override;
    void disConnect() override;
    void fetchTuple(IPFiveTuple &tuple) override;

    // NetReading interface
public:
    void onProc(const QByteArray &arry) override;

private:
    /**
     * @brief onStateChanged 状态改变通知。
     * @param connected
     */
    void onStateChanged(bool connected);
private:
    UdpChannel *m_chn;
    IPFiveTuple m_ipTuple;
    bool m_state;
    QThread *m_innerThread; // 单独线程。

};

#endif // UDPTUNNEL_H
