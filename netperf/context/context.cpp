﻿#include <context/context.h>
#include <data/replypacket.h>
#include <data/startargs.h>
#include <data/stopargs.h>
#include <state/statefactory.h>
#include <pointer/basepointer.h>
#include "QsLog.h"

Context::Context(BasePointer *pointer)
    : QObject(Q_NULLPTR)
{
    m_pointer = pointer;
    m_startArgs = Q_NULLPTR;
    m_state = Q_NULLPTR;
    m_bServer = true;
    m_procPercent = 100.0f;
}

Context::Context(BasePointer *pointer, const StartArgs &args)
    : QObject(Q_NULLPTR)
{
    m_pointer = pointer;
    m_startArgs = new StartArgs(args);
    m_state = Q_NULLPTR;
    m_bServer = false;
}

Context::~Context()
{
    if(m_state)
    {
        delete m_state;
        m_state = nullptr;
    }
    if(m_startArgs)
    {
        delete m_startArgs;
        m_startArgs = Q_NULLPTR;
    }
}

BaseState *Context::state() const
{
    return m_state;
}

void Context::changeState(BaseState *state)
{
    if(state == Q_NULLPTR)
    {
        QLOG_TRACE() << QObject::tr("Change final!");
    }else {
        QLOG_TRACE() << QString(QObject::tr("Change state to %1") ).arg(state->stateName());
    }

    if(m_state)
    {
        delete m_state;
        m_state = nullptr;
    }
    if(state)
    {
        m_state = state;
        m_state->process(); // 改变状态完毕后，执行当前状态。
    }else {
        this->deleteLater();  //   释放当前对象。
    }
}

BasePointer *Context::pointer() const
{
    return m_pointer;
}

float Context::procPercent() const
{
    return m_procPercent;
}

void Context::setProcPercent(float procPercent)
{
    if(abs(procPercent - m_procPercent) >= 0.1f) //控制调用频率。
    {
        m_procPercent = procPercent;
        emit workProcess(procPercent);
    }
}

bool Context::buildDataLink(const IPFiveTuple &args)
{
    Q_ASSERT(m_pointer);
    return m_pointer->buildDataLink(args);
}

void Context::add(PointerCallback *callback)
{
    Q_ASSERT(m_pointer);
    m_pointer->add(callback);
}

void Context::remove(PointerCallback *callback)
{
    Q_ASSERT(m_pointer);
    m_pointer->remove(callback);
}

void Context::disConnect(bool bCmd)
{
    Q_ASSERT(m_pointer);
    m_pointer->disConnect(bCmd);
}

void Context::close(bool bCmd)
{
    Q_ASSERT(m_pointer);
    m_pointer->close(bCmd);
}

bool Context::start()
{
    if(!m_startArgs)
    {
        return false;
    }
    Q_ASSERT(m_pointer);
    StartArgs args = *m_startArgs;
    IPFiveTuple tuple = args.ipTuple();
    if(m_pointer->buildDataLink(tuple)) // 建立数据链路。
    {
        tuple.reverse();
        args.setIpTuple(tuple);  // 反转后通过指令链路发送到远端。
        return m_pointer->start(args);
    }else {
        QLOG_WARN() << QObject::tr("build datalink failed, ");
        return false;
    }
}

void Context::init(const InitArgs &args)
{
    if(m_state)
    {
        delete m_state;
    }
    BaseState::StateType type = m_bServer ?
                BaseState::InitServer : BaseState::InitClient;
    StateFactory factory(type, m_bServer);
    m_state = factory.create(this);
    m_state->process();
    m_pointer->init(args);
}

bool Context::request(const RequestPacket &pack)
{
    Q_ASSERT(m_pointer);
    m_pointer->request(pack);
    return true;
}

bool Context::reply(const ReplyPacket &pack)
{
    Q_ASSERT(m_pointer);
    m_pointer->reply(pack);
    return true;
}

bool Context::ready()
{
    Q_ASSERT(m_pointer);
    m_pointer->ready();
    return true;
}

void Context::stop(const StopArgs &args)
{
    Q_ASSERT(m_pointer);

    QLOG_INFO() << QString(QObject::tr("Receive stop %1")).arg(args.toString());
    m_pointer->stop(args);
}

StartArgs *Context::startArgs() const
{
    return m_startArgs;
}
