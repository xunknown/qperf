﻿#ifndef CONTEXTFACTORY_H
#define CONTEXTFACTORY_H

class Context;
class BasePointer;
class StartArgs;
/**
 * @brief The ContextFactory class
 *  上下文工厂，负责创建上下文实例。
 */
class ContextFactory
{
public:
    /**
     * @brief ContextFactory 客户端的初始方法。
     * @param pointer
     */
    ContextFactory(BasePointer *pointer);
    /**
     * @brief setArgs 客户端设置启动参数。
     * @param args 启动参数。
     */
    void setArgs(const StartArgs &args);
    ~ContextFactory();
    Context *create();
private:
    BasePointer *m_pointer;
    StartArgs *m_startArgs;
    bool m_bServer;
};

#endif // CONTEXTFACTORY_H
