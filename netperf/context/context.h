﻿#ifndef CONTEXT_H
#define CONTEXT_H

#include <QObject>


class BaseState;
class StartArgs;
class StopArgs;
class ReplyPacket;
class RequestPacket;
class BasePointer;
class InitArgs;
class IPFiveTuple;
class PointerCallback;
class ReadyArgs;
/**
 * @brief The BaseContext class 表示性能上下文。
 */
class Context : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief BaseContext 服务端的初始构造。
     * @param pointer
     */
    Context(BasePointer *pointer);
    /**
     * @brief BaseContext 客户端的初始构造
     * @param pointer
     * @param args
     */
    Context(BasePointer *pointer, const StartArgs &args);
    virtual ~Context() override;

public:
    /**
     * @brief state
     * @return 返回当前状态。
     */
    BaseState *state() const;
    /**
     * @brief changeState 改变当前状态。
     * @param state
     */
    void changeState(BaseState *state);
    /**
     * @brief buildLink 建立连接。
     * @param args IP参数。
     */
    bool buildDataLink(const IPFiveTuple &args);
    /**
     * @brief add 添加监听对象。
     * @param callback 监听实例。
     */
    void add(PointerCallback *callback);
    /**
     * @brief remove 移除监听对象。
     * @param callback 监听实例。
     */
    void remove(PointerCallback *callback);
    /**
     * @brief disConnect 断开当前连接。
     * @param bCmd 是否指令链路。
     */
    void disConnect(bool bCmd);
    /**
     * @brief close 彻底关闭。
     * @param bCmd 是否指令链路。
     */
    void close(bool bCmd);
public:
    /**
     * @brief start 不带参数的stat
     */
    bool start();
    /**
     * @brief init 初始。
     * @param args 初始参数。
     */
    void init(const InitArgs &args);
    /**
     * @brief request 请求数据包。
     * @param pack
     * @return 成功返回true。
     */
    bool request(const RequestPacket &pack);
    /**
     * @brief reply 回复数据包。
     * @param pack
     * @return 成功返回true。
     */
    bool reply(const ReplyPacket &pack);
    /**
     * @brief ready 准备就绪参数。
     * @param args
     * @return
     */
    bool ready();
    /**
     * @brief stop 停止。
     * @param args
     */
    void stop(const StopArgs &args);

    /**
     * @brief startArgs 获取启动参数。
     * @return
     */
    StartArgs *startArgs() const;
    /**
     * @brief procPercent  处理占比。
     * @return 返回处理占比。
     */
    float procPercent() const;
    /**
     * @brief setProcPercent 设置处理占比。
     * @param procPercent 占比。
     */
    void setProcPercent(float procPercent);
signals:
    /**
     * @brief workProcess 工作进度信号。
     * @param percent
     */
    void workProcess(float percent);
protected:
    /**
     * @brief pointer 获取远端节点实例。
     * @return
     */
    BasePointer *pointer() const;
protected:
    BaseState *m_state; // 当前状态。
    BasePointer *m_pointer; // 远端节点。
    StartArgs *m_startArgs; // 开始参数。
    bool m_bServer; // 是否服务端。
    float m_procPercent; // 处理占比。
};


#endif // CONTEXT_H
