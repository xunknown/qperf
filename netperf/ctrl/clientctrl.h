﻿#ifndef CLIENTCTRL_H
#define CLIENTCTRL_H

#include "basectrl.h"

#include <data/initargs.h>
#include <data/startargs.h>

/**
 * @brief The ClientCtrl class 客户端的控制逻辑。
 */
class ClientCtrl : public BaseCtrl
{
public:
    ClientCtrl(const InitArgs &initArgs, const StartArgs &startArgs);

    // BaseCtrl interface
public:
    bool start() override;
private slots:
    /**
     * @brief workProcess 进度槽。
     * @param percent 进度（比例）。
     */
    void workProcess(float percent);
private:
    InitArgs m_initArgs;
    StartArgs m_startArgs;
};

#endif // CLIENTCTRL_H
