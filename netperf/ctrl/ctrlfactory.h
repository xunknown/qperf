﻿#ifndef CTRLFACTORY_H
#define CTRLFACTORY_H

#include "abstractctrlfactory.h"

#include <data/startargs.h>


class BaseCtrl;
class InitArgs;
/**
 * @brief The CtrlFactory class
 *   控制器创建工厂。
 */
class CtrlFactory : public AbstractCtrlFactory
{
public:
    /**
     * @brief CtrlFactory 服务端的构造。
     */
    CtrlFactory();
    /**
     * @brief CtrlFactory 客户端的构造。
     * @param startArgs 客户端的启动参数。
     */
    CtrlFactory(const StartArgs &startArgs);
    BaseCtrl *create(const InitArgs &initArgs) override;
};

#endif // CTRLFACTORY_H
