﻿#include "fakectrlfactory.h"
#include "testclientctrl.h"
#include "testserverctrl.h"


FakeCtrlFactory::FakeCtrlFactory(bool bServer, const StartArgs &startArgs)
    : AbstractCtrlFactory(startArgs)
{
    m_bServer = bServer;
    m_startArgs = startArgs;
    m_bServer = false;
}

BaseCtrl *FakeCtrlFactory::create(const InitArgs &initArgs)
{
    if(m_bServer)
    {
        return new TestServerCtrl(initArgs, m_startArgs);
    }
    return new TestClientCtrl(initArgs, m_startArgs);
}
