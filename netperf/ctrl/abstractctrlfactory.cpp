﻿#include "abstractctrlfactory.h"

AbstractCtrlFactory::AbstractCtrlFactory()
{
    m_bServer = true;
}

AbstractCtrlFactory::AbstractCtrlFactory(const StartArgs &startArgs)
{
    m_startArgs = startArgs;
    m_bServer = false;
}

AbstractCtrlFactory::~AbstractCtrlFactory()
{

}
