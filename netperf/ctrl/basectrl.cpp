﻿#include "basectrl.h"

#include <context/context.h>
#include <pointer/basepointer.h>
#include <QCoreApplication>

BaseCtrl::BaseCtrl(QObject *parent)
    :QObject (parent)
{
    m_context = Q_NULLPTR;
    m_pointer = Q_NULLPTR;
}

BaseCtrl::~BaseCtrl()
{
    if(m_pointer)
    {
        delete m_pointer;
        m_pointer = Q_NULLPTR;
    }
    if(m_context)  // 删除
    {
        m_context->deleteLater();
    }
}
void BaseCtrl::exit()
{
    qApp->exit();
}


