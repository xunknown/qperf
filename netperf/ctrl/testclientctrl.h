﻿#ifndef TESTCLIENTCTRL_H
#define TESTCLIENTCTRL_H

#include "basectrl.h"

#include <data/initargs.h>
#include <data/startargs.h>

/**
 * @brief The TestClientCtrl class 测试用。
 * TODO 测试的逻辑。
 */
class TestClientCtrl : public BaseCtrl
{
public:
    TestClientCtrl(const InitArgs &initArgs, const StartArgs &startArgs);

    // BaseCtrl interface
public:
    bool start() override;
private:
    InitArgs m_initArgs;
    StartArgs m_startArgs;
};

#endif // TESTCLIENTCTRL_H
