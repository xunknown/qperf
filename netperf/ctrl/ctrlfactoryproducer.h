﻿#ifndef CTRLFACTORYPRODUCER_H
#define CTRLFACTORYPRODUCER_H

#include <data/startargs.h>

class AbstractCtrlFactory;
/**
 * @brief The CtrlFactoryProducer class
 * 控制器的抽象工厂创建者。
 */
class CtrlFactoryProducer
{
public:
    /**
     * @brief CtrlFactoryProducer 真实服务端控制器工厂的构造。
     */
    CtrlFactoryProducer();
    /**
     * @brief CtrlFactoryProducer 真实客户端控制器工厂的构造。
     * @param args  客户端启动参数。
     */
    CtrlFactoryProducer(const StartArgs &args);
    /**
     * @brief CtrlFactoryProducer 虚拟的控制器工厂构造。
     * @param bServer 是否服务端。
     * @param args 启动参数。
     */
    CtrlFactoryProducer(bool bServer, const StartArgs &args);
    /**
     * @brief CtrlFactoryProducer
     * @param bServer 是否服务端。
     * @param bFake 是否虚拟。
     * @param args 启动参数。
     */
    CtrlFactoryProducer(bool bServer, bool bFake, const StartArgs &args);
    /**
     * @brief create 创建服务端的抽象工厂的实例。
     * @return 返回工厂实例。
     */
    AbstractCtrlFactory *create();
private:
    bool m_bServer; // 是否服务端。
    bool m_bFake; // 是否虚拟。
    StartArgs m_args; // 启动参数。

};

#endif // CTRLFACTORYPRODUCER_H
