﻿#ifndef RATECONTROL_H
#define RATECONTROL_H

#include <QTime>

/**
 * @brief 表示采样率控制类。
 */
class RateControl
{
public:
    /**
     * @brief RateControl 默认构造。
     */
    RateControl();
    /**
     * @brief RateControl 初始化。
     * @param sampling  采样率（Hz）。
     */
    RateControl(int sampling);
    /**
     * @brief 设置允许数据包容错大小（默认50ms的数据）根据实际业务修改。
     * @param allowDataSize 容错数据包大小。
     */
    void setAlowErrorCount(int allowErrorCount);
    /**
     * @brief 获取允许数据包容错大小
     * @return
     */
    int allowErrorCount();
    /**
     * @brief 启动。
     */
    void start();
    /**
     * @brief 重新启动。
     */
    void restart();
    /**
     * @brief 添加样点。(非线程安全)
     * @param sampleCount 样点数。
     * @return 返回需等待时间(ms）。
     */
    int addSample(int sampleCount);

    /**
     * @brief sampling 获取采样率。
     * @return
     */
    int sampling() const;
    /**
     * @brief setSampling 设置采样率。
     * @param sampling
     */
    void setSampling(int sampling);

private:

    /**
     * @brief 获取总运行时间。
     * @return 返回运行时间（ms）
     */
    qint64 totalSpan() const;
private:
    const int m_resetInteval = 7200;  //自动重置时间间隔。
    int m_sampling;   //数据采样率
    int m_allowErrorCode;    //允许数据包容错大小
    QTime m_startTime;  //启动时间
    qint64 m_totalSampleCount; //读取到的总数据大小
};

#endif // CONFIGARGS_H
