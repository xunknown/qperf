﻿#ifndef CALLBACKSERVER_H
#define CALLBACKSERVER_H

#include <QMutex>
#include <QVector>

/**
 * 表示线程安全的集合模板类。
 */
template<typename T>
class ThreadSafeSet
{
public:
    ThreadSafeSet()
    {
        m_mutex = new QMutex;
    }

    virtual ~ThreadSafeSet()
    {
        delete m_mutex;
    }
    /**
     * @brief add 添加回调。
     * @param callback
     */
    void add(T *callback)
    {
        lock();
        m_callbackSet.append(callback);
        unlock();
    }
    /**
     * @brief remove 移除回调。
     * @param callback
     */
    void remove(T *callback)
    {
        lock();
        m_callbackSet.removeAll(callback);
        unlock();
    }
    /**
     * @brief removeAll 移除所有回调
     */
    void removeAll()
    {
        lock();
        m_callbackSet.clear();
        unlock();
    }
    /**
     * @brief execEach 遍历并执行。
     */
    template<typename DoSth>
    void execEach(DoSth &func)
    {
        QVector<T *> tmp;
        lock();
        tmp.append(m_callbackSet);
        unlock();

        foreach(auto item, tmp)
        {
            func(item);
        }
    }
    void lock()
    {
        m_mutex->lock();
    }
    void unlock()
    {
        m_mutex->unlock();
    }
protected:
    QMutex *m_mutex;
    QVector<T *> m_callbackSet;
};

#endif // CALLBCKSERVER_H
