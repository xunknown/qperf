﻿#ifndef IPFIVETUPLE_H
#define IPFIVETUPLE_H

#include <QHostAddress>


/**
 * @brief The IPFiveTuple class
 * 表示IP五元组。
 */
class IPFiveTuple
{
public:
    enum Type
    {
        Type_Udp = 0, // Udp连接。
        Type_TcpServer = 1, // Tcp服务端。
        Type_TcpClient = 2 // Tcp客户端。
    };

public:
    IPFiveTuple();
    /**
     * @brief IPFiveTuple
     * @param type
     * @param remoteAddr
     * @param remotePort
     * @param localAddr
     * @param localPort
     */
    IPFiveTuple(IPFiveTuple::Type type, QHostAddress &remoteAddr, quint16 remotePort,
                QHostAddress &localAddr, quint16 localPort);
    /**
     * @brief type 获取类型。
     * @return
     */
    IPFiveTuple::Type type() const;
    /**
     * @brief setType 设置类型。
     * @param type
     */
    void setType(const IPFiveTuple::Type &type);

    /**
     * @brief remotePort 获取远端端口。
     * @return 返回远端端口。
     */
    quint16 remotePort() const;
    /**
     * @brief setRemotePort 设置远端端口。
     * @param remotePort
     */
    void setRemotePort(const quint16 &remotePort);

    /**
     * @brief localAddr  获取本地地址。
     * @return 返回本地地址。
     */
    QHostAddress localAddr() const;
    /**
     * @brief setLocalAddr 设置本地地址。
     * @param localAddr
     */
    void setLocalAddr(const QHostAddress &localAddr);

    /**
     * @brief localPort  获取本地端口。
     * @return 本地端口。
     */
    quint16 localPort() const;
    /**
     * @brief setLocalPort 设置本地端口。
     * @param localPort
     */
    void setLocalPort(const quint16 &localPort);

    /**
     * @brief remoteAddr 获取远端地址。
     * @return
     */
    QHostAddress remoteAddr() const;
    /**
     * @brief setRemoteAddr 设置远端地址。
     * @param remoteAddr
     */
    void setRemoteAddr(const QHostAddress &remoteAddr);

    /**
     * @brief reverse 反转五元组中的远端-本地属性。
     * 本地地址 <-> 远端地址；
     * 本地端口 <-> 远端端口；
     * TcpServer <-> TcpClient。
     */
    void reverse();
    /**
     * @brief toString 获取格式化字符串。
     * @return 返回当前对象的格式化字符串。
     */
    QString toString() const;
private:
    Type m_type;
    QHostAddress m_remoteAddr;
    quint16 m_remotePort;
    QHostAddress m_localAddr;
    quint16 m_localPort;

};

#endif // IPFIVETUPLE_H
