﻿#ifndef STOPARGSTEST_H
#define STOPARGSTEST_H

/**
 * @brief The StopArgsTest class
 *   测试停止指令封装。
 */
class StopArgsTest
{
public:
    StopArgsTest();
    /**
     * @brief run_data
     *  准备测试数据。
     */
    void run_data();
    /**
     * @brief run 执行测试。
     */
    void run();
};

#endif // STOPARGSTEST_H
