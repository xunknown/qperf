﻿#include "commonpacktest.h"

#include <data/commonpack.h>

#include <serialize/commonpackparser.h>
#include <serialize/commonpackwriter.h>
#include <QtTest>

CommonPackTest::CommonPackTest()
{

}

void CommonPackTest::run()
{
    QFETCH(int, expectDataSize);
    QFETCH(char, expectChar);
    QFETCH(int, type);

    QByteArray testAry(expectDataSize - 16, expectChar);
    CommonPack testData;
    testData.setType(CommonPack::Type(type));
    testData.setContent(testAry);

    Data dest;
    // 测试数据编码
    testCommonPackWriter(testData, expectDataSize, dest);
    //  测试解析数据
    testCommonParser(dest, testData); // 解析数据长度。
}

void CommonPackTest::run_data()
{
    QTest::addColumn<int>("expectDataSize");
    QTest::addColumn<char>("expectChar");
    QTest::addColumn<int>("type");

    QTest::addRow("R1") << (10000 + 16) << '\x0' << int(CommonPack::Type_Start);
    QTest::addRow("R2") << (10000000 + 16) << '\x0' << int(CommonPack::Type_Stop);
    QTest::addRow("R3") << (16) << '\xFF' << int(CommonPack::Type_Request);
    QTest::addRow("R4") << (1 +16) << '\xCC' << int(CommonPack::Type_Reply);
}

void CommonPackTest::testCommonParser(Data &src, CommonPack &expect)
{
    CommonPackParser parser;
    CommonPack destPack;
    QByteArray destAry = src.buffer();
    int parserSize = parser.parse(src, destPack);
    QCOMPARE(parserSize, destAry.size()); // 解析数据长度。
    QCOMPARE(destPack.type(), expect.type()); // 数据类型

    QByteArray actualContent = destPack.content();
    QByteArray expectContent = expect.content();
    int actualContentSize = actualContent.size();
    QCOMPARE(actualContentSize, expectContent.size()); // 数据内容长度
    // 验证数据内容。
    for (int i = 0; i < actualContentSize; i++) {
        QCOMPARE(actualContent[i], expectContent[i]);
    }

    //  测试解析数据。 数据长度小于数据包的情况。
    Data dest2(src.buffer().mid(0, 2));
    parserSize = parser.parse(dest2, destPack);
    QCOMPARE(parserSize, 0); // 解析数据长度。

    //  测试解析数据。 数据包不正确的情况。
    Data dest3(src.buffer());
    dest3.setOffset(1);
    parserSize = parser.parse(dest3, destPack);
    QVERIFY(parserSize <= 0); // 解析数据长度。

    // 测试解析数据。 数据包大于协议约定的长度。
    char *pDestAry = destAry.data();
    int testMaxSize = parser.MaxContentSize + 1; // 测试最大值。
    qToBigEndian<int>(testMaxSize, &pDestAry[6]); // 重置。
    Data dest4(destAry);
    parserSize = parser.parse(dest4, destPack);
    QVERIFY(parserSize < 0);
}

void CommonPackTest::testCommonPackWriter(CommonPack &src, int expectSize, Data &dest)
{
    CommonPackWriter writer;
    int size = writer.retrieveSize(src);
    QCOMPARE(size, expectSize); // 测试数据包长度。

    bool bWrited = writer.write(src, dest);
    QVERIFY(bWrited); // 测试编码数据长度。
    QByteArray destAry = dest.buffer();

    int actualSize = dest.size();
    QCOMPARE(actualSize, expectSize); // 测试长度。
    QCOMPARE(dest.offset(), 0); // 测试偏移。

    //  测试起始和结束数据
    QByteArray expectContent = src.content();
    for (int i = 0; i < expectSize - 16; i++) {
        QCOMPARE(destAry[16 + i], expectContent[i]); // 验证数据内容。
    }
}
