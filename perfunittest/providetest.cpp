#include "providetest.h"

#include <QTest>
#include <QThreadPool>
/**
 * @brief The ReadRunner class
 * 读取线程的对象。
 */
class ReadRunner: public QRunnable{
public:
    ReadRunner(int total, ProvideTest *owner);
    // QRunnable interface
public:
    void run();
private:
    ProvideTest *m_owner;
    int m_total;
};
/**
 * @brief The WriteRunner class
 * 写入的线程对象。
 */
class WriteRunner: public QRunnable{
public:
    WriteRunner(int total, ProvideTest *owner);
    // QRunnable interface
public:
    void run();
private:
    ProvideTest *m_owner;
    int m_total;
};
ProvideTest::ProvideTest()
{

}

void ProvideTest::run()
{
    int total = 10000000; // 测试数量，越多越容易重现。

    m_list.reserve(total);
    m_list2.reserve(total);
    ReadRunner *read = new ReadRunner(total, this);
    QThreadPool pool;
    pool.start(read);

    WriteRunner *write = new WriteRunner(total, this);
    pool.start(write);
}

void ProvideTest::runTest(QVector<int> &src)
{
    int size = src.size();
    if(size < 0)
    {
        QString msg = QString("QVector failed size:%1;cur size:%2").arg(size).arg(src.size());
        QFAIL(msg.toUtf8().data());
    }
}

void ProvideTest::runTest(QList<int> &src)
{
    int size = src.size();
    if(size < 0)
    {
        QString msg = QString("QList failed size:%1;cur size:%2").arg(size).arg(src.size());
        QFAIL(msg.toUtf8().data());
    }
}

void ReadRunner::run()
{
    int count = m_total;
    auto fun1 = [=](QVector<int> &item){this->m_owner->runTest(item);};
    auto fun2 = [=](QList<int> &item){this->m_owner->runTest(item);};

    for(int i = 0; i < count; i++){
        fun1(m_owner->m_list);
        fun2(m_owner->m_list2);
    }
}

void WriteRunner::run()
{
    int count = m_total;
    for(int i = 0; i < count; i++){
        m_owner->m_list.append(i);
        m_owner->m_list2.append(i);
    }
}

ReadRunner::ReadRunner(int total, ProvideTest *owner)
{
    m_total = total;
    m_owner = owner;
}

WriteRunner::WriteRunner(int total, ProvideTest *owner)
{
    m_total = total;
    m_owner = owner;
}
