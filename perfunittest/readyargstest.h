﻿#ifndef READYARGSTEST_H
#define READYARGSTEST_H 
/**
 * @brief 表示准备完毕参数封装测试类。
 */
class ReadyArgsTest
{
public:
    ReadyArgsTest();
    virtual ~ReadyArgsTest();
    /**
    * @biref 执行准备数据。
    */
    void run_data();
    /**
    * @biref 执行测试。
    */
    void run();
}; 
#endif // READYARGSTEST_H
