#ifndef PROVIDETEST_H
#define PROVIDETEST_H

#include <QList>
#include <QVector>


/**
 * @brief The ProvideTest class
 * qt容器的的线程安全测试。
 */
class ProvideTest
{
public:
    ProvideTest();
    void run();
private:
    void runTest(QVector<int> &src);
    void runTest(QList<int> &src);
private:
    QVector<int> m_list; // QVector 容器。
    QList<int> m_list2; // QList容器。
    friend class ReadRunner;
    friend class WriteRunner;
};

#endif // PROVIDETEST_H
